#include <cstdlib>
#include <iostream>
#include <math.h>
#include <armadillo>

using namespace std;
using namespace arma;

mat loadFromFile(string fileName)
{
	ifstream file;
	file.open(fileName);
	if (!file.good()) return zeros<mat>(2,2);

	int lines = 0;

	while (!file.eof())
	{
		string line;
		getline(file, line);
		lines++;
	}
	file.close();

	file.open(fileName);
	vec x(lines);
	vec y(lines);
	double numberX, numberY;
	char comma;
	int index = 0;

	while (!file.eof())
	{
		file >> numberX;
		x(index) = numberX;
		file >> comma;
		file >> numberY;
		y(index) = numberY;
		index++;
	}
	file.close();

	mat returnValues;
	returnValues.insert_cols(0, x);
	returnValues.insert_cols(1, y);
	returnValues.print("loaded values:");

	return returnValues;
}

mat loadFromFileV2(string fileName)
{
	ifstream file;
	file.open(fileName);
	if (!file.good()) return zeros<mat>(2, 2);

	int lines = 0;

	while (!file.eof())
	{
		string line;
		getline(file, line);
		lines++;
	}
	file.close();

	file.open(fileName);
	vec x(lines);
	vec y(lines);
	vec z(lines);
	double numberX, numberY, numberZ;
	char comma;
	int index = 0;

	while (!file.eof())
	{
		file >> numberX;
		x(index) = numberX;
		file >> comma;
		file >> numberY;
		y(index) = numberY;
		file >> comma;
		file >> numberZ;
		z(index) = numberZ;
		index++;
	}
	file.close();

	mat returnValues;
	returnValues.insert_cols(0, x);
	returnValues.insert_cols(1, y);
	returnValues.insert_cols(2, z);
	returnValues.print("loaded values:");

	return returnValues;
}

mat loadFromFileV3(string fileName)
{
	ifstream file;
	file.open(fileName);
	if (!file.good()) return zeros<mat>(2, 2);

	int lines = 0;

	while (!file.eof())
	{
		string line;
		getline(file, line);
		lines++;
	}
	file.close();

	file.open(fileName);
	vec x(lines);
	vec y(lines);
	vec z(lines);
	vec v(lines);
	double numberX, numberY, numberZ, numberV;
	char comma;
	int index = 0;

	while (!file.eof())
	{
		file >> numberX;
		x(index) = numberX;
		file >> comma;
		file >> numberY;
		y(index) = numberY;
		file >> comma;
		file >> numberZ;
		z(index) = numberZ;
		file >> comma;
		file >> numberV;
		v(index) = numberV;
		index++;
	}
	file.close();

	mat returnValues;
	returnValues.insert_cols(0, x);
	returnValues.insert_cols(1, y);
	returnValues.insert_cols(2, z);
	returnValues.insert_cols(3, v);
	returnValues.print("loaded values:");

	return returnValues;
}

vector<double> unconfinedExplosion(string fileName)
{
	// Physical Explosion (P = overpressure):
	// P(R) = a / R
	// R - distance from the source [m]
	// P - overpressure [bar] 
	// Example:
	// R, P
	// 10, 0.2
	// 20, 0.15

	int maxIter = 10000;
	mat a0 = zeros<vec>(1);
	mat loadedValues = loadFromFile(fileName);
	vec x = loadedValues.col(0); //x = { 10, 20, 30, 50, 250 };
	//x << 10 << 20 << 30 << 50 << 250;
	vec y = loadedValues.col(1);; //y = { 0.2, 0.15, 0.11, 0.07, 0.014 };
	//y << 0.2 << 0.15 << 0.11 << 0.07 << 0.014;
	double accuracy = 1E-4;
	mat a = a0;
	int i = 0;

	mat J;

	// 1./x
	vec jak1 = 1 / x;
	//jak1.print("jak1:");

	J.insert_cols(0, jak1);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	//int A = 1; 
	//mat A = eye<mat>(1, 1);
	//cout << "A(eye(1)): " << A << endl;
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		// Jakobian
		J.shed_col(0);
		vec jak1 = 1 / x;

		J.insert_cols(0, jak1);

		// f = a(1,i)./x;
		f = a(0, i) / x;

		// atest(:,1) = a(:,i) + ([J' * J + m]^-1) * J' * (y-f);
		atest = a.col(i) + (J.t() * J + m).i() * J.t() * (y - f);

		// ftest = atest(1,1)./x;
		ftest = atest(0, 0) / x;

		// roznica1 = sum((y - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(y - f)));

		// roznica2=sum((y - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(y - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= accuracy) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl << endl;
				vector<double> returnVector = { a(0, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> physicalExplosion(string fileName)
{
	// Physical Explosion (P = overpressure):
	// P(R) = (a + b) / (R^c + d)
	// R - distance from the source [m]
	// P - overpressure [bar] 
	// Example:
	// R, P
	// 10, 0.2
	// 20, 0.15

	int maxIter = 10000;
	mat a0 = zeros<vec>(4);
	mat loadedValues = loadFromFile(fileName);
	vec x = loadedValues.col(0); //x = { 10, 20, 30, 50, 250 };
	//x << 10 << 20 << 30 << 50 << 250;
	vec y = loadedValues.col(1); //y = { 0.2, 0.15, 0.11, 0.07, 0.014 };
	//y << 0.2 << 0.15 << 0.11 << 0.07 << 0.014;
	double accuracy = 1E-4;
	mat a = a0;
	int i = 0;

	mat J;

	// 1./(x.^a(3, i) + a(4, i))
	vec jak1 = 1 / (pow(x, a(2, i)) + a(3, i));
	//jak1.print("jak1:");
	// 1./(x.^a(3, i) + a(4, i))
	vec jak2 = 1 / (pow(x, a(2, i)) + a(3, i));
	// -((a(1, i) + a(2, i)).*log(x).*x.^a(3, i)) ./ power(x.^a(3, i) + a(4, i), 2)
	vec jak3 = -((a(0, i) + a(1, i)) * log(x) % pow(x, a(2, i))) / (square(pow(x, a(2, i)) + a(3, i)));
	// -(a(1, i)+a(2, i)) ./ power(x.^a(3, i)+a(4, i), 2)
	vec jak4 = -(a(0, i) + a(1, i)) / (square(pow(x, a(2, i)) + a(3, i)));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	J.insert_cols(3, jak4);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(4, 4);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		// Jakobian
		J.shed_cols(0, 3);
		vec jak1 = 1 / (pow(x, a(2, i)) + a(3, i));
		vec jak2 = 1 / (pow(x, a(2, i)) + a(3, i));
		vec jak3 = -((a(0, i) + a(1, i)) * log(x) % pow(x, a(2, i))) / (square(pow(x, a(2, i)) + a(3, i)));
		vec jak4 = -(a(0, i) + a(1, i)) / (square(pow(x, a(2, i)) + a(3, i)));
		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);
		J.insert_cols(3, jak4);

		// f = (a(1, i) + a(2, i))./(x.^a(3, i)+a(4, i));
		f = (a(0, i) + a(1, i)) / (pow(x, a(2, i)) + a(3, i));
		
		// atest(:, 1) = a(:, i) + ([J' * J + m * eye(4)]^-1) * J' * (y - f);
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (y - f);

		// ftest = (atest(1, 1) + atest(2, 1))./(x.^atest(3, 1) + atest(4, 1));
		ftest = (atest(0, 0) + atest(1, 0)) / (pow(x, atest(2, 0)) + atest(3, 0));

		// roznica1 = sum((y - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(y - f)));

		// roznica2=sum((y - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(y - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);
			
			i++;
			m /= 10;
			
			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);
			
			if ((dokladnosc2(i - 1) <= accuracy) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl;
				cout << "b = " << a(1, i) << endl;
				cout << "c = " << a(2, i) << endl;
				cout << "d = " << a(3, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i), a(3, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> fireball(string fileName)
{
	// Fireball (I = thermal radiation):
	// I(R) = (a + b) / (R^2 + c)
	// R - distance from the source [m]
	// I - max radiation at ground [kw/m^2] 
	// Example:
	// R, I
	// 1, 27.6
	// 50, 27.1

	int maxIter = 10000;
	mat a0 = ones<vec>(3);
	mat loadedValues = loadFromFile(fileName);
	vec x = loadedValues.col(0); //x = { 1, 50, 101, 201, 301 };
	//x << 1 << 50 << 101 << 201 << 301;
	vec y = loadedValues.col(1); //y = { 27.6, 27.1, 25.7, 21.35, 16.6 };
	//y << 27.6 << 27.1 << 25.7 << 21.35 << 16.6;
	double accuracy = 1E-3;
	mat a = a0;
	int i = 0;

	mat J;

	// 1./ (x.^2 + a(3,i))
	vec jak1 = 1 / (square(x) + a(2, i));
	//jak1.print("jak1:");
	// 1 ./ (x.^2 + a(3,i))
	vec jak2 = 1 / (square(x) + a(2, i));
	// -(a(1,i) + a(2,i)) ./ power(x.^2 + a(3,i), 2)
	vec jak3 = -(a(0, i) + a(1, i)) / (square(square(x) + a(2, i)));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(3, 3);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		// Jakobian
		J.shed_cols(0, 2);
		vec jak1 = 1 / (square(x) + a(2, i));
		vec jak2 = 1 / (square(x) + a(2, i));
		vec jak3 = -(a(0, i) + a(1, i)) / (square(square(x) + a(2, i)));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);
		
		// f = (a(1,i) + a(2,i)) ./ (x.^2 + a(3,i));
		f = (a(0, i) + a(1, i)) / (square(x) + a(2, i));

		// atest(:,1) = a(:,i) + ([J' * J + m * eye(3)]^-1) * J' * (y-f); 
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (y - f);

		// ftest = (atest(1,1) + atest(2,1)) ./ (x.^2 + atest(3,1));
		ftest = (atest(0, 0) + atest(1, 0)) / (square(x) + atest(2, 0));

		// roznica1 = sum((y - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(y - f)));

		// roznica2=sum((y - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(y - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= accuracy) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl;
				cout << "b = " << a(1, i) << endl;
				cout << "c = " << a(2, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> jetFire(string fileName)
{
	// Jet fire (I = thermal radiation):
	// I(R, T) = a / (1 + b * z + c * z^2)
	// where:
	// z = |X|
	// X = RcosT
	// I(R, T) = a / (1 + b * |RcosT| + c * (RcosT)^2)
	// R - distance from the source [m]
	// I - max radiation at ground [kw/m^2] 
	// Example:
	// R, I
	// 1, 27.6
	// 50, 27.1

	int maxIter = 20000;
	mat a0 = zeros<vec>(3);
	mat loadedValues = loadFromFile(fileName);
	vec x = loadedValues.col(0); //x = { 1, 50, 101, 201, 301 };
	//x << 1 << 50 << 101 << 201 << 301;
	vec y = loadedValues.col(1); //y = { 27.6, 27.1, 25.7, 21.35, 16.6 };
	//y << 27.6 << 27.1 << 25.7 << 21.35 << 16.6;
	double T = 10;
	double accuracy = 1E-4;
	mat a = a0;
	int i = 0;

	mat J;

	// 1./(1 + a(2,i).*abs(x.*cosd(T)) + a(3,i).*(x.*cosd(T)).^2)
	vec jak1 = 1 / (1 + a(1, i) * abs(x * cos(T)) + a(2, i) * square(x * cos(T)));
	//jak1.print("jak1:");
	// -( a(2,i).*abs(x.*cosd(T)))./((1+a(2,i).*abs(x.*cosd(T))+a(3,i).*(x.*cosd(T)).^2)).^2
	vec jak2 = -(a(1, i) * abs(x * cos(T))) / square(1 + a(1, i ) * abs(x * cos(T)) + a(2, i) * square(x * cos(T)));
	// -(a(1,i).*(x.*cosd(T)).^2)./(1+a(2,i).*abs(x.*cosd(T))+a(3,i).*(x.*cosd(T)).^2)
	vec jak3 = -(a(0, i) * square(x * cos(T))) / (1 + a(1, i) * abs(x * cos(T)) + a(2, i) * square(x * cos(T)));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(3, 3);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		// Jakobian
		J.shed_cols(0, 2);
		vec jak1 = 1 / (1 + a(1, i) * abs(x * cos(T)) + a(2, i) * square(x * cos(T)));
		vec jak2 = -(a(1, i) * abs(x * cos(T))) / square(1 + a(1, i) * abs(x * cos(T)) + a(2, i) * square(x * cos(T)));
		vec jak3 = -(a(0, i) * square(x * cos(T))) / (1 + a(1, i) * abs(x * cos(T)) + a(2, i) * square(x * cos(T)));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);

		// f = a(1,i)./(1 + a(2,i).*abs(x*cosd(T)) + a(3,i).*(x*cosd(T)).^2);
		f = a(0, i) / (1 + a(1, i) * abs(x * cos(T)) + a(2, i) * square(x * cos(T)));

		// atest(:,1) = a(:,i) + ([J' * J + m * eye(3)]^-1) * J' * (y-f);
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (y - f);

		// ftest = atest(1,1)./(1 + atest(2,1).*abs(x*cosd(T)) + atest(3,1).*(x*cosd(T)).^2);
		ftest = atest(0, 0) / (1 + atest(1, 0) * abs(x * cos(T)) + atest(2, 0) * square(x * cos(T)));

		// roznica1 = sum((y - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(y - f)));

		// roznica2=sum((y - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(y - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= accuracy) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl;
				cout << "b = " << a(1, i) << endl;
				cout << "c = " << a(2, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> poolFire(string fileName)
{
	int maxIter = 20000;
	mat a0 = ones<vec>(6);
	mat loadedValues = loadFromFileV2(fileName);
	vec x = loadedValues.col(0);
	//x << 2 << 2 << 2 << 2 << 10 << 10 << 10 << 10 << 25 << 25 << 25 << 25;
	vec y = loadedValues.col(2);
	//y << 33.2 << 32.8 << 31.8 << 30.3 << 10 << 9.86 << 9.42 << 8.79 << 1.9 << 1.84 << 1.8 << 1.78;
	vec T = loadedValues.col(1);
	//T << 0 << 15 << 30 << 45 << 0 << 15 << 30 << 45 << 0 << 15 << 30 << 45;
	double accuracy = 1E-3;
	mat a = a0;
	int i = 0;

	// przeliczenie na radiany
	// T=T*pi/180;
	T = T * datum::pi / 180;

	// dane zlogarytmowane po przekszta�ceniu
	vec y2 = log(y);

	mat J;

	// (T.^2).*(a(2,i)-a(3,i).*log(x./a(6,i))-a(4,i).*power(log(x./a(6,i)),2)-a(5,i).*power(log(x./a(6,i)),3));  
	vec jak1 = square(T) % (a(1, i) - a(2, i) * log(x / a(5, i)) - a(3, i) * square(log(x / a(5, i))) - a(4, i) * pow(log(x / a(5, i)), 3));
	//jak1.print("jak1:");

	// 1-a(1,i).*T.^2
	vec jak2 = 1 - a(0, i) * square(T);

	// -j2.*log(x./a(6,i));
	vec jak3 = -jak2 % log(x / a(5, i));
	
	// -j2.*power(log(x./a(6,i)),2);
	vec jak4 = -jak2 % square(log(x / a(5, i)));
	
	// -j2.*power(log(x./a(6,i)),3);
	vec jak5 = -jak2 % pow(log(x / a(5, i)), 3);
	
	// j2.*(a(3,i)./a(6,i) +2.*a(4,i).*log(x./a(6,i))./a(6,i) +3.*a(5,i).*power(log(x./a(6,i)),2)./a(6,i));
	vec jak6 = jak2 % (a(2, i) / a(5, i) + 2 * a(3, i) * log(x / a(5, i)) / a(5, i) + 3 * a(4, i) * square(log(x / a(5, i))) / a(5, i));
	
	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	J.insert_cols(3, jak4);
	J.insert_cols(4, jak5);
	J.insert_cols(5, jak6);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(6, 6);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		// Jakobian
		J.shed_cols(0, 5);
		vec jak1 = square(T) % (a(1, i) - a(2, i) * log(x / a(5, i)) - a(3, i) * square(log(x / a(5, i))) - a(4, i) * pow(log(x / a(5, i)), 3));
		vec jak2 = 1 - a(0, i) * square(T);
		vec jak3 = -jak2 % log(x / a(5, i));
		vec jak4 = -jak2 % square(log(x / a(5, i)));
		vec jak5 = -jak2 % pow(log(x / a(5, i)), 3);
		vec jak6 = jak2 % (a(2, i) / a(5, i) + 2 * a(3, i) * log(x / a(5, i)) / a(5, i) + 3 * a(4, i) * square(log(x / a(5, i))) / a(5, i));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);
		J.insert_cols(3, jak4);
		J.insert_cols(4, jak5);
		J.insert_cols(5, jak6);

		// f= (1-a(1,i).*T.^2) .* (a(2,i) - a(3,i).*log(x./a(6,i)) - a(4,i).*power(log(x./a(6,i)),2) - a(5,i).*power(log(x./a(6,i)),3));
		f = (1 - a(0, i) * square(T)) % (a(1, i) - a(2, i) * log(x / a(5, i)) - a(3, i) * square(log(x / a(5, i))) - a(4, i) * pow(log(x / a(5, i)),3));

		// atest(:,1) = a(:,i) + ([J'*J+m*eye(6)]^-1)*J'*(y2-f);   
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (y2 - f);

		// ftest=(1-atest(1,1).*T.^2).*(atest(2,1)-atest(3,1).*log(x./atest(6,1))-atest(4,1).*power(log(x./atest(6,1)),2)-atest(5,1).*power(log(x./atest(6,1)),3));  
		ftest = (1 - atest(0, 0) * square(T)) % (atest(1, 0) - atest(2, 0) * log(x / atest(5, 0)) - atest(3, 0) * square(log(x / atest(5, 0))) - atest(4, 0) * pow(log(x / atest(5, 0)), 3));

		// roznica1 = sum((y2 - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(y2 - f)));

		// roznica2=sum((y2 - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(y2 - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= accuracy) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl << endl;
				cout << "b = " << a(1, i) << endl << endl;
				cout << "c = " << a(2, i) << endl << endl;
				cout << "d = " << a(3, i) << endl << endl;
				cout << "e = " << a(4, i) << endl << endl;
				cout << "f = " << a(5, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i), a(3, i), a(4, i), a(5, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> gasJetDispersionAB(vec R_data, vec Ca_data, double T, double dokladnosc, int maxIter)
{
	mat a0 = ones<vec>(2);
	mat a = a0;
	int i = 0;

	mat J;

	// power(R_data.*cosd(T),a(2,i))
	vec jak1 = pow(R_data * cos(T), a(1, i));
	//jak1.print("jak1:");

	// a(1,i).*log(R_data.*cosd(T)).*power(R_data.*cosd(T),a(2,i))
	vec jak2 = a(0, i) * log(R_data * cos(T)) % pow(R_data * cos(T), a(1, i));
	
	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(2, 2);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		J.shed_cols(0, 1);
		vec jak1 = pow(R_data * cos(T), a(1, i));
		vec jak2 = a(0, i) * log(R_data * cos(T)) % pow(R_data * cos(T), a(1, i));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);

		// f=a(1,i).*power(R_data.*cosd(T),a(2,i));  
		f = a(0, i) * pow(R_data * cos(T), a(1, i));

		// atest(:,1)=a(:,i)+([J'*J+m*eye(2)]^-1)*J'*(Ca_data-f); 
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (Ca_data - f);

		// ftest=atest(1,1).*power(R_data.*cosd(T),atest(2,1));
		ftest = atest(0, 0) * pow(R_data * cos(T), atest(1, 0));

		// roznica1 = sum((Ca_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(Ca_data - f)));

		// roznica2 = sum((Ca_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(Ca_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl << endl;
				cout << "b = " << a(1, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> gasJetDispersionCD(vec R_data, vec dR_data, double T, double dokladnosc, int maxIter)
{
	mat a0 = ones<vec>(2);
	mat a = a0;
	int i = 0;

	mat J;

	// power(R_data.*cosd(T),aa(2,i)) 
	vec jak1 = pow(R_data * cos(T), a(1, i));
	//jak1.print("jak1:");

	// aa(1,i).*log(R_data.*cosd(T)).*power(R_data.*cosd(T),aa(2,i))
	vec jak2 = a(0, i) * log(R_data * cos(T)) % pow(R_data * cos(T), a(1, i));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(2, 2);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		J.shed_cols(0, 1);
		vec jak1 = pow(R_data * cos(T), a(1, i));
		vec jak2 = a(0, i) * log(R_data * cos(T)) % pow(R_data * cos(T), a(1, i));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);

		// f=aa(1,i).*power(R_data.*cosd(T),aa(2,i)); 
		f = a(0, i) * pow(R_data * cos(T), a(1, i));

		// aatest(:,1)=aa(:,i)+([J'*J+m*eye(2)]^-1)*J'*(dR_data-f);
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (dR_data - f);

		// ftest=aatest(1,1).*power(R_data.*cosd(T),aatest(2,1));
		ftest = atest(0, 0) * pow(R_data * cos(T), atest(1, 0));

		// roznica1 = sum((dR_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(dR_data - f)));

		// roznica2 = sum((dR_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(dR_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "c = " << a(0, i) << endl << endl;
				cout << "d = " << a(1, i) << endl << endl;
				vector<double> returnVector = { a(0, i),  a(1, i) };
				return returnVector;
				break;
			}
		}
	}
}

vector<double> gasJetDispersionEFGH(vec R_data, vec ZR_data, double T, double dokladnosc, int maxIter)
{
	mat a0 = zeros<vec>(4);
	mat a = a0;
	int i = 0;

	mat J;

	// exp(aaa(2,i).*R_data.*cosd(T))
	vec jak1 = exp(a(1, i) * R_data * cos(T));
	//jak1.print("jak1:");

	// aaa(1,i).*exp(aaa(2,i).*R_data.*cosd(T)).*R_data.*cosd(T)
	vec jak2 = a(0, i) * exp(a(1, i) * R_data * cos(T)) % R_data * cos(T);

	// exp(aaa(4,i).*R_data.*cosd(T))
	vec jak3 = exp(a(3, i) * R_data * cos(T));

	// aaa(3,i).*exp(aaa(4,i).*R_data.*cosd(T)).*R_data.*cosd(T)
	vec jak4 = a(2, i) * exp(a(3, i) * R_data * cos(T)) % R_data * cos(T);

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	J.insert_cols(3, jak4);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(4, 4);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;
	
	while (true)
	{
		J.shed_cols(0, 3);
		vec jak1 = exp(a(1, i) * R_data * cos(T));
		vec jak2 = a(0, i) * exp(a(1, i) * R_data * cos(T)) % R_data * cos(T);
		vec jak3 = exp(a(3, i) * R_data * cos(T));
		vec jak4 = a(2, i) * exp(a(3, i) * R_data * cos(T)) % R_data * cos(T);

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);
		J.insert_cols(3, jak4);
		
		// f=aaa(1,i).*exp(aaa(2,i).*R_data.*cosd(T))+aaa(3,i).*exp(aaa(4,i).*R_data.*cosd(T));
		f = a(0, i) * exp(a(1, i) * R_data * cos(T)) + a(2, i) * exp(a(3, i) * R_data * cos(T));

		// aaatest(:,1)=aaa(:,i)+([J'*J+m*eye(4)]^-1)*J'*(ZR_data-f); 
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (ZR_data - f);

		// ftest=aaatest(1).*exp(aaatest(2).*R_data.*cosd(T))+aaatest(3).*exp(aaatest(4).*R_data.*cosd(T));
		ftest = atest(0, 0) * exp(atest(1, 0) * R_data * cos(T)) + atest(2, 0) * exp(atest(3, 0) * R_data * cos(T));

		// roznica1 = sum((ZR_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(ZR_data - f)));

		// roznica2 = sum((ZR_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(ZR_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "e = " << a(0, i) << endl << endl;
				cout << "f = " << a(1, i) << endl << endl;
				cout << "g = " << a(2, i) << endl << endl;
				cout << "h = " << a(3, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i), a(3, i) };
				return returnVector;
				break;
			}
		}
	}
}

vector<double> gasJetDispersion(string fileName)
{	
	mat loadedValues = loadFromFileV3(fileName);
	vec R_data = loadedValues.col(0);
	//R_data << 10.1 << 60.9 << 210.8;
	vec Ca_data = loadedValues.col(1);
	//Ca_data << 28300 << 1615 << 195.5;
	vec dR_data = loadedValues.col(2);
	//dR_data << 2.6 << 10.6 << 30.5;
	vec ZR_data = loadedValues.col(3);
	//ZR_data << 16.1 << 15.6 << 14.8;
	
	double T = 0;

	double Z = max(ZR_data);

	// y_data = Ca_data.*exp(-4*Z*(ZR_data.^2+R_data*sind(T)^2)./(1.35*dR_data.^2 .*ZR_data ));
	vec y_data = Ca_data % exp(-4 * Z * (square(ZR_data) + R_data * pow(sin(T),2)) / (1.35 * square(dR_data) % ZR_data));
	//y_data.print("y_data:");

	int maxIter = 200;
	
	double accuracy = 1E-2;

	vector<double> AB = gasJetDispersionAB(R_data, Ca_data, T, accuracy, maxIter);
	vector<double> CD = gasJetDispersionCD(R_data, dR_data, T, accuracy, maxIter);
	vector<double> EFGH = gasJetDispersionEFGH(R_data, ZR_data, T, accuracy, maxIter);

	vector<double> returnVector = { AB[0], AB[1], CD[0], CD[1], EFGH[0], EFGH[1], EFGH[2], EFGH[3] };
	return returnVector;
	
}

vector<double> heavyGasDispersionContinuousReleaseTI(vec R_data, vec sR_data, double dokladnosc, int maxIter)
{
	vec s2R_data = tan(datum::pi / 2 - datum::pi * sR_data) / 10000;
	mat a0 = ones<vec>(2);
	mat a = a0;
	int i = 0;

	mat J;

	// -R_data.*sin(a(1,i))
	vec jak1 = -R_data * sin(a(0, i));
	//jak1.print("jak1:");

	// -1*ones(3,1)
	vec jak2 = -1 * ones<vec>(3);

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(2, 2);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		J.shed_cols(0, 1);
		vec jak1 = -R_data * sin(a(0, i));
		vec jak2 = -1 * ones<vec>(3);

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);

		// f=R_data*cos(a(1,i))-a(2,i);
		f = R_data * cos(a(0, i)) - a(1, i);

		// atest(:,1)=a(:,i)+([J'*J+m*eye(2)]^-1)*J'*(s2R_data-f);
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (s2R_data - f);

		// ftest=R_data*cos(atest(1,1))-atest(2,1);
		ftest = R_data * cos(atest(0, 0)) - atest(1, 0);

		// roznica1 = sum((s2R_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(s2R_data - f)));

		// roznica2 = sum((s2R_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(s2R_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "T = " << a(0, i) << endl << endl;
				cout << "i = " << a(1, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> heavyGasDispersionContinuousReleaseFGH(vec R_data, vec sR_data, vec bR_data, double T, double dokladnosc, int maxIter)
{
	mat a0 = zeros<vec>(3);
	mat a = a0;
	int i = 0;

	mat J;

	// abs(sR_data)
	vec jak1 = abs(sR_data);
	//jak1.print("jak1:");

	// abs(sR_data.*log(R_data.*cos(TT)))
	vec jak2 = abs(sR_data % log(R_data * cos(T)));

	// abs(sR_data.*power(log(R_data.*cos(TT)),2))
	vec jak3 = abs(sR_data % square(log(R_data * cos(T))));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(3, 3);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		J.shed_cols(0, 2);
		vec jak1 = abs(sR_data);
		vec jak2 = abs(sR_data % log(R_data * cos(T)));
		vec jak3 = abs(sR_data % square(log(R_data * cos(T))));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);

		// f=abs(sR_data.*(aa(1,i)+aa(2,i).*log(R_data.*cos(TT))+aa(3,i)* power(log(R_data.*cos(TT)),2))); 
		f = abs(sR_data % (a(0, i) + a(1, i) * log(R_data * cos(T)) + a(2, i) * square(log(R_data * cos(T)))));
		
		// aatest(:,1)=aa(:,i)+([J'*J+m*eye(3)]^-1)*J'*(bR_data-f);
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (bR_data - f);

		// ftest=abs(sR_data.*(aatest(1,1)+aatest(2,1).*log(R_data.*cos(TT))+aatest(3,1)* power(log(R_data.*cos(TT)),2)));
		ftest = abs(sR_data % (atest(0, 0) + atest(1, 0) * log(R_data * cos(T)) + atest(2, 0) * square(log(R_data * cos(T)))));

		// roznica1 = sum((bR_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(bR_data - f)));

		// roznica2 = sum((bR_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(bR_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "f = " << a(0, i) << endl << endl;
				cout << "g = " << a(1, i) << endl << endl;
				cout << "h = " << a(2, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> heavyGasDispersionContinuousReleaseAC(vec R_data, vec Ca_data, vec bR_data, double T, double dokladnosc, int maxIter)
{
	// Ca2_data=log(Ca_data)-bR_data.*log(R_data.*cos(TT));
	vec Ca2_data = log(Ca_data) - bR_data % log(R_data * cos(T));
	mat a0 = ones<vec>(2);
	mat a = a0;
	int i = 0;

	mat J;

	// ones(3,1)
	vec jak1 = ones<vec>(3);
	//jak1.print("jak1:");

	// 1./sqrt(log(R_data.*cos(TT)))
	vec jak2 = 1 / sqrt(log(R_data * cos(T)));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(2, 2);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		J.shed_cols(0, 1);
		vec jak1 = ones<vec>(3);
		vec jak2 = 1 / sqrt(log(R_data * cos(T)));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);

		// f=aaa(1,i)+aaa(2,i)./sqrt(log(R_data.*cos(TT)));
		f = a(0, i) + a(1, i) / sqrt(log(R_data * cos(T)));

		// aaatest(:,1)=aaa(:,i)+([J'*J+m*eye(2)]^-1)*J'*(Ca2_data-f); 
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (Ca2_data - f);

		// ftest=aaatest(1,1)+aaatest(2,1)./sqrt(log(R_data.*cos(TT)));
		ftest = atest(0, 0) + atest(1, 0) / sqrt(log(R_data * cos(T)));

		// roznica1 = sum((Ca2_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(Ca2_data - f)));

		// roznica2 = sum((Ca2_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(Ca2_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl << endl;
				cout << "c = " << a(1, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> heavyGasDispersionContinuousReleaseV1(string fileName)
{
	mat loadedValues = loadFromFileV3(fileName);
	vec R_data = loadedValues.col(0);
	//R_data << 20 << 40 << 60;
	vec Ca_data = loadedValues.col(1);
	//Ca_data << 733.7 << 230.9 << 112.9;
	vec sR_data = loadedValues.col(2);
	//sR_data << 17.84 << 28.2 << 36.8;
	vec bR_data = loadedValues.col(3);
	//bR_data << 260.77 << 325.2 << 363.8;

	double T_data = 0;
	T_data = T_data * datum::pi / 180;

	int maxIter = 10000;
	double accuracy = 1E-6;

	double T_param = heavyGasDispersionContinuousReleaseTI(R_data, sR_data, accuracy, maxIter)[0];
	vector<double> FGH = heavyGasDispersionContinuousReleaseFGH(R_data, sR_data, bR_data, T_param, accuracy, maxIter);
	vector<double> AC = heavyGasDispersionContinuousReleaseAC(R_data, Ca_data, bR_data, T_param, accuracy, maxIter);

	vector<double> returnVector = { AC[0], AC[1], FGH[0], FGH[1], FGH[2] };
	return returnVector;
}

vector<double> heavyGasDispersionContinuousReleaseABC(vec R_data, vec Ca_data, vec bR_data, double T, double dokladnosc, int maxIter)
{
	// Ca2_data=log(Ca_data);
	vec Ca2_data = log(Ca_data);
	mat a0 = ones<vec>(3);
	mat a = a0;
	int i = 0;

	mat J;

	// ones(3,1)
	vec jak1 = ones<vec>(3);
	//jak1.print("jak1:");

	// bR_data.*log(R_data.*cos(TT))
	vec jak2 = bR_data % log(R_data * cos(T));

	// 1./sqrt(log(R_data.*cos(TT))
	vec jak3 = 1 / sqrt(log(R_data * cos(T)));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(3, 3);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		J.shed_cols(0, 2);
		vec jak1 = ones<vec>(3);
		vec jak2 = bR_data % log(R_data * cos(T));
		vec jak3 = 1 / sqrt(log(R_data * cos(T)));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);

		// f=aaa(1,i)+ aaa(2,i).*bR_data.*log(R_data.*cos(TT))+aaa(3,i)./sqrt(log(R_data.*cos(TT)));
		f = a(0, i) + a(1, i) * bR_data % log(R_data * cos(T)) + a(2, i) / sqrt(log(R_data * cos(T)));

		// aaatest(:,1)=aaa(:,i)+([J'*J+m*eye(3)]^-1)*J'*(Ca2_data-f);
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (Ca2_data - f);

		// ftest=aaatest(1,1)+aaatest(2,1).*bR_data.*log(R_data.*cos(TT))+aaatest(3,1)./sqrt(log(R_data.*cos(TT))) ;
		ftest = atest(0, 0) + atest(1, 0) * bR_data % log(R_data * cos(T)) + atest(2, 0) / sqrt(log(R_data * cos(T)));

		// roznica1 = sum((Ca2_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(Ca2_data - f)));

		// roznica2 = sum((Ca2_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(Ca2_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl << endl;
				cout << "b = " << a(1, i) << endl << endl;
				cout << "c = " << a(2, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i) };
				return returnVector;
				break;
			}
		}
	}
}

vector<double> heavyGasDispersionContinuousReleaseV2(string fileName)
{
	mat loadedValues = loadFromFileV3(fileName);
	vec R_data = loadedValues.col(0);
	//R_data << 20 << 40 << 60;
	vec Ca_data = loadedValues.col(1);
	//Ca_data << 733.7 << 230.9 << 112.9;
	vec sR_data = loadedValues.col(2);
	//sR_data << 17.84 << 28.2 << 36.8;
	vec bR_data = loadedValues.col(3);
	//bR_data << 260.77 << 325.2 << 363.8;

	double T_data = 0;
	T_data = T_data * datum::pi / 180;

	int maxIter = 10000;
	double accuracy = 1E-6;

	double T_param = heavyGasDispersionContinuousReleaseTI(R_data, sR_data, accuracy, maxIter)[0];
	vector<double> FGH = heavyGasDispersionContinuousReleaseFGH(R_data, sR_data, bR_data, T_param, accuracy, maxIter);
	vector<double> ABC = heavyGasDispersionContinuousReleaseABC(R_data, Ca_data, bR_data, T_param, accuracy, maxIter);

	vector<double> returnVector = { ABC[0], ABC[1], ABC[2], FGH[0], FGH[1], FGH[2] };
	return returnVector;
}

vector<double> heavyGasDispersionContinuousReleaseTIV3(vec R_data, vec dR_data, double T_data, double dokladnosc, int maxIter)
{
	//vec s2R_data = tan(datum::pi / 2 - datum::pi * sR_data) / 10000;
	mat a0 = ones<vec>(2);
	mat a = a0;
	int i = 0;

	mat J;

	// power(R_data.*cos(T_data),a(2,i))
	vec jak1 = pow(R_data * cos(T_data), a(1,i));
	//jak1.print("jak1:");

	// a(1,i).*power(R_data.*cos(T_data),a(2,i)).*log(R_data.*cos(T_data))
	vec jak2 = a(0, i) * pow(R_data * cos(T_data), a(1, i)) % log(R_data * cos(T_data));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(2, 2);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		J.shed_cols(0, 1);
		vec jak1 = pow(R_data * cos(T_data), a(1, i));
		vec jak2 = a(0, i) * pow(R_data * cos(T_data), a(1, i)) % log(R_data * cos(T_data));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);

		// f=a(1,i).*power(R_data.*cos(T_data),a(2,i));
		f = a(0, i) * pow(R_data * cos(T_data), a(1, i));

		// atest(:,1)=a(:,i)+([J'*J+m*eye(2)]^-1)*J'*(dR_data-f); 
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (dR_data - f);

		// ftest=atest(1,1).*power(R_data.*cos(T_data),atest(2,1));
		ftest = atest(0, 0) * pow(R_data * cos(T_data), atest(1, 0));

		// roznica1 = sum((dR_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(dR_data - f)));

		// roznica2 = sum((dR_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(dR_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "T = " << a(0, i) << endl << endl;
				cout << "i = " << a(1, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> heavyGasDispersionContinuousReleaseFGHI(vec R_data, vec bR_data, double T, double dokladnosc, int maxIter)
{
	mat a0 = zeros<vec>(4);
	mat a = a0;
	int i = 0;

	// s=(pi()-2*atan(10000*(R_data.*cos(T_data)-aa(4,i))))/2*pi();
	mat s = (datum::pi - 2 * atan(10000 * (R_data * cos(T) - a(3, i)))) / 2 * datum::pi;

	mat J;

	// abs(s) 
	vec jak1 = abs(s);
	//jak1.print("jak1:");

	// abs(s.*log(R_data.*cos(T_data)))
	vec jak2 = abs(s % log(R_data * cos(T)));

	// abs(s.*power(log(R_data.*cos(T_data)),2))
	vec jak3 = abs(s % square(log(R_data * cos(T))));

	// abs(10000*(aa(1,i)+aa(2,i).*log(R_data.*cos(T_data))+aa(3,i).*power(log(R_data.*cos(T_data)),2))./(pi()*(1+power(10000*(R_data.*cos(T_data)-aa(4,i)),2))))
	vec jak4 = abs(10000 * (a(0, i) + a(1, i) * log(R_data * cos(T)) + a(2, i) * square(log(R_data * cos(T)))) / (datum::pi * (1 + square(10000 * (R_data * cos(T) -a(3, i))))));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	J.insert_cols(3, jak4);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(4, 4);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		s = (datum::pi - 2 * atan(10000 * (R_data * cos(T) - a(3, i)))) / 2 * datum::pi;

		J.shed_cols(0, 3);
		vec jak1 = abs(s);
		vec jak2 = abs(s % log(R_data * cos(T)));
		vec jak3 = abs(s % square(log(R_data * cos(T))));
		vec jak4 = abs(10000 * (a(0, i) + a(1, i) * log(R_data * cos(T)) + a(2, i) * square(log(R_data * cos(T)))) / (datum::pi * (1 + square(10000 * (R_data * cos(T) - a(3, i))))));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);
		J.insert_cols(3, jak4);

		// f=abs(s.*(aa(1,i)+aa(2,i).*log(R_data.*cos(T_data))+aa(3,i).*power(log(R_data.*cos(T_data)),2)));  
		f = abs(s % (a(0, i) + a(1, i) * log(R_data * cos(T)) + a(2, i) * square(log(R_data * cos(T)))));

		// aatest(:,1)=aa(:,i)+([J'*J+m*eye(4)]^-1)*J'*(bR_data-f);
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (bR_data - f);

		// ftest=abs(s.*(aatest(1,1)+aatest(2,1).*log(R_data.*cos(T_data))+aatest(3,1).*power(log(R_data.*cos(T_data)),2)))
		ftest = abs(s % (atest(0, 0) + atest(1, 0) * log(R_data * cos(T)) + atest(2, 0) * square(log(R_data * cos(T)))));

		// roznica1 = sum((bR_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(bR_data - f)));

		// roznica2 = sum((bR_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(bR_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "f = " << a(0, i) << endl;
				cout << "g = " << a(1, i) << endl;
				cout << "h = " << a(2, i) << endl;
				cout << "i = " << a(3, i) << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i), a(3, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> heavyGasDispersionContinuousReleaseABCV3(vec R_data, vec Ca_data, vec bR_data, double T, double dokladnosc, int maxIter)
{
	// Ca2_data=log(Ca_data);
	vec Ca2_data = log(Ca_data);
	mat a0 = ones<vec>(3);
	mat a = a0;
	int i = 0;

	mat J;

	// ones(3,1)
	vec jak1 = ones<vec>(3);
	//jak1.print("jak1:");

	// bR_data.*log(R_data.*cos(T_data))
	vec jak2 = bR_data % log(R_data * cos(T));

	// sqrt(log(R_data.*cos(T_data)))
	vec jak3 = sqrt(log(R_data * cos(T)));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(3, 3);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		J.shed_cols(0, 2);
		vec jak1 = ones<vec>(3);
		vec jak2 = bR_data % log(R_data * cos(T));
		vec jak3 = sqrt(log(R_data * cos(T)));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);

		// aaa(1,i)+ aaa(2,i).*bR_data.*log(R_data.*cos(T_data))+aaa(3,i).*sqrt(log(R_data.*cos(T_data)))
		f = a(0, i) + a(1, i) * bR_data % log(R_data * cos(T)) + a(2, i) * sqrt(log(R_data * cos(T)));

		// aaatest(:,1)=aaa(:,i)+([J'*J+m*eye(3)]^-1)*J'*(Ca2_data-f); 
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (Ca2_data - f);

		//  ftest=aaatest(1,1)+aaatest(2,1).*bR_data.*log(R_data.*cos(T_data))+aaatest(3,1).*sqrt(log(R_data.*cos(T_data))) ;
		ftest = atest(0, 0) + atest(1, 0) * bR_data % log(R_data * cos(T)) + atest(2, 0) * sqrt(log(R_data * cos(T)));

		// roznica1 = sum((Ca2_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(Ca2_data - f)));

		// roznica2 = sum((Ca2_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(Ca2_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl << endl;
				cout << "b = " << a(1, i) << endl << endl;
				cout << "c = " << a(2, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i) };
				return returnVector;
				break;
			}
		}
	}
}

vector<double> heavyGasDispersionContinuousReleaseV3(string fileName)
{
	mat loadedValues = loadFromFileV3(fileName);
	vec R_data = loadedValues.col(0);
	//R_data << 20 << 40 << 60;
	vec Ca_data = loadedValues.col(1);
	//Ca_data << 733.7 << 230.9 << 112.9;
	vec dR_data = loadedValues.col(2);
	//sR_data << 17.84 << 28.2 << 36.8;
	vec bR_data = loadedValues.col(3);
	//bR_data << 260.77 << 325.2 << 363.8;

	double T_data = 0;
	T_data = T_data * datum::pi / 180;

	int maxIter = 10000;
	double accuracy = 1E-6;

	double T_param = heavyGasDispersionContinuousReleaseTIV3(R_data, dR_data, T_data, accuracy, maxIter)[0];
	vector<double> FGHI = heavyGasDispersionContinuousReleaseFGHI(R_data, bR_data, T_data, accuracy, maxIter);
	vector<double> ABC = heavyGasDispersionContinuousReleaseABCV3(R_data, Ca_data, bR_data, T_data, accuracy, maxIter);

	vector<double> returnVector = { ABC[0], ABC[1], ABC[2], FGHI[0], FGHI[1], FGHI[2], FGHI[3] };
	return returnVector;
}

vector<double> heavyGasDispersionforCatastrophicReleaseABCD(vec R_data, vec dR_data, double T, double dokladnosc, int maxIter)
{
	// d2R_data=log10(dR_data);
	vec d2R_data = log10(dR_data);
	mat a0 = ones<vec>(4);
	mat a = a0;
	int i = 0;

	mat J;

	vec X2 = log10(R_data * cos(T));

	// power(X2,0.1)
	vec jak1 = pow(X2, 0.1);
	//jak1.print("jak1:");

	//  power(X2,0.2)
	vec jak2 = pow(X2, 0.2);

	//  power(X2,0.3)
	vec jak3 = pow(X2, 0.3);

	// ones(3,1)
	vec jak4 = ones<vec>(3);

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	J.insert_cols(3, jak4);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(4, 4);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while (true)
	{
		J.shed_cols(0, 3);
		vec jak1 = pow(X2, 0.1);
		vec jak2 = pow(X2, 0.2);
		vec jak3 = pow(X2, 0.3);
		vec jak4 = ones<vec>(3);

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);
		J.insert_cols(3, jak4);

		// f=a(1,i).*power(X2,0.1)+a(2,i).*power(X2,0.2)+a(3,i).*power(X2,0.3)+a(4,i);
		f = a(0, i) * pow(X2, 0.1) + a(1, i) * pow(X2, 0.2) + a(2, i) * pow(X2, 0.3) + a(3, i);

		//atest(:,1)=a(:,i)+([J'*J+m*eye(4)]^-1)*J'*(d2R_data-f);   
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (d2R_data - f);

		// ftest=atest(1,1).*power(X2,0.1)+atest(2,1).*power(X2,0.2)+atest(3,1).*power(X2,0.3)+atest(4,1);
		ftest = atest(0, 0) * pow(X2, 0.1) + atest(1, 0) * pow(X2, 0.2) + atest(2, 0) * pow(X2, 0.3) + atest(3, 0);

		// roznica1 = sum((d2R_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(d2R_data - f)));

		// roznica2 = sum((d2R_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(d2R_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "a = " << a(0, i) << endl << endl;
				cout << "b = " << a(1, i) << endl << endl;
				cout << "c = " << a(2, i) << endl << endl;
				cout << "d = " << a(3, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i), a(3, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> heavyGasDispersionforCatastrophicReleaseEFGHJLM(vec R_data, vec Ca_data, double T, double dokladnosc, int maxIter)
{
	// C2a_data=2*pi*log(Ca_data);
	vec C2a_data = 2 * datum::pi * log(Ca_data);
	mat a0 = ones<vec>(7);
	mat a = a0;
	int i = 0;

	mat J;

	// X0=log(R_data.*cos(T_data)./aa(7,i));%zmienna pomocnicza
	vec X0 = log(R_data * cos(T) / a(6, i));
	// X3=2*atan(aa(3,i).*(X0-aa(4,i)));%zmienna pomocnicza
	vec X3 = 2 * atan(a(2, i) * (X0 - a(3, i)));
	// X4=1+power(aa(3,i).*(X0-aa(4,i)),2);%zmienna pomocnicza
	vec X4 = 1 + square(a(2, i) * (X0 - a(3, i)));

	// pi+X3
	vec jak1 = datum::pi + X3;
	//jak1.print("jak1:");

	// j1.*X0
	vec jak2 = jak1 % X0;

	// 2*(X0-aa(4,i)).*(aa(1,i)+aa(2,i).*X0)./X4-2*(X0-aa(4,i)).*(aa(5,i)+aa(6,i).*X0)./X4;
	vec jak3 = 2 * (X0 - a(3, i)) % (a(0, i) + a(1, i) * X0) / X4 - 2 * (X0 - a(3, i)) % (a(4, i) + a(5, i) * X0) / X4;

	// -2*aa(3,i).*(aa(1,i)+aa(2,i).*X0-aa(5,i)-aa(6,i).*X0)./X4;
	vec jak4 = -2 * a(2, i) * (a(0, i) + a(1, i) * X0 - a(4, i) - a(5, i) * X0) / X4;

	// pi-X3
	vec jak5 = datum::pi - X3;

	// j5.*X0
	vec jak6 = jak5 % X0;

	// (aa(1,i)+aa(2,i).*X0).*(-2*aa(3,i)./aa(7,i))./X4+  (pi+X3)./(-aa(7,i)) + (aa(5,i)+aa(6,i).*X0)*(2*aa(3,i)./aa(7,i))./X4 + (pi-X3)./(-aa(7,i));
	vec jak7 = (a(0, i) + a(1, i) * X0) * (-2 * a(2, i) / a(6, i)) / X4 + (datum::pi + X3) / (-a(6, i)) + (a(4, i) + a(5, i) * X0) * (2 * a(2, i) / a(6, i)) / X4 + (datum::pi - X3) / (-a(6, i));

	J.insert_cols(0, jak1);
	J.insert_cols(1, jak2);
	J.insert_cols(2, jak3);
	J.insert_cols(3, jak4);
	J.insert_cols(4, jak5);
	J.insert_cols(5, jak6);
	J.insert_cols(6, jak7);
	//J.print("J:");

	double m = max(((mat)(J.t() * J)).diag()) / 10;

	mat f, ftest;
	mat atest;
	mat A = eye<mat>(7, 7);
	double roznica1, roznica2;
	mat dokladnosc2;
	mat tmp;

	while(true)
	{
		vec X0 = log(R_data * cos(T) / a(6, i));
		vec X3 = 2 * atan(a(2, i) * (X0 - a(3, i)));
		vec X4 = 1 + square(a(2, i) * (X0 - a(3, i)));

		J.shed_cols(0, 6);
		vec jak1 = datum::pi + X3;
		vec jak2 = jak1 % X0;
		vec jak3 = 2 * (X0 - a(3, i)) % (a(0, i) + a(1, i) * X0) / X4 - 2 * (X0 - a(3, i)) % (a(4, i) + a(5, i) * X0) / X4;
		vec jak4 = -2 * a(2, i) * (a(0, i) + a(1, i) * X0 - a(4, i) - a(5, i) * X0) / X4;
		vec jak5 = datum::pi - X3;
		vec jak6 = jak5 % X0;
		vec jak7 = (a(0, i) + a(1, i) * X0) * (-2 * a(2, i) / a(6, i)) / X4 + (datum::pi + X3) / (-a(6, i)) + (a(4, i) + a(5, i) * X0) * (2 * a(2, i) / a(6, i)) / X4 + (datum::pi - X3) / (-a(6, i));

		J.insert_cols(0, jak1);
		J.insert_cols(1, jak2);
		J.insert_cols(2, jak3);
		J.insert_cols(3, jak4);
		J.insert_cols(4, jak5);
		J.insert_cols(5, jak6);
		J.insert_cols(6, jak7);

		// f=(pi+X3).*(aa(1,i)+aa(2,i).*X0)+ (pi-X3).* (aa(5,i)+aa(6,i).*X0);
		f = (datum::pi + X3) % (a(0, i) + a(1, i) * X0) + (datum::pi - X3) % (a(4, i) + a(5, i) * X0);

		// aatest(:,1)=aa(:,i)+([J'*J+m*eye(7)]^-1)*J'*(C2a_data-f);
		atest = a.col(i) + (J.t() * J + m * A).i() * J.t() * (C2a_data - f);

		//X0 = log(R_data.*cos(T_data). / aatest(7, 1)); %zmienna pomocnicza
		X0 = log(R_data * cos(T) / atest(6, 0));

		//X3 = 2 * atan(aatest(3, 1).*(X0 - aatest(4, 1))); %zmienna pomocnicza
		X3 = 2 * atan(atest(2, 0) * (X0 - atest(3, 0)));

		// ftest=(pi+X3).*(aatest(1,1)+aatest(2,1).*X0)+ (pi-X3).* (aatest(5,1)+aatest(6,1).*X0);
		ftest = (datum::pi + X3) % (atest(0, 0) + atest(1, 0) * X0) + (datum::pi - X3) % (atest(4, 0) + atest(5, 0) * X0);

		// roznica1 = sum((C2a_data - f).^2); %suma kwadrat�w r�nic dla poprzedniego rozwi�zania
		roznica1 = sum((square(C2a_data - f)));

		// roznica2 = sum((C2a_data - ftest).^2); %suma kwadrat�w r�nic dla testowego rozwi�zania
		roznica2 = sum((square(C2a_data - ftest)));

		if (roznica2 > roznica1)
		{
			m *= 10;
		}
		else
		{
			// a(:,i+1 ) = atest(:,1);
			a.insert_cols(i + 1, atest);

			i++;
			m /= 10;

			// dokladnosc2(i) = roznica2;
			tmp << roznica2 << endr;
			dokladnosc2.insert_cols(i - 1, tmp);

			if ((dokladnosc2(i - 1) <= dokladnosc) || (i > maxIter))
			{
				cout << endl << "Final results" << endl;
				cout << "e = " << a(0, i) << endl << endl;
				cout << "f = " << a(1, i) << endl << endl;
				cout << "g = " << a(2, i) << endl << endl;
				cout << "h = " << a(3, i) << endl << endl;
				cout << "j = " << a(4, i) << endl << endl;
				cout << "l = " << a(5, i) << endl << endl;
				cout << "m = " << a(6, i) << endl << endl;
				vector<double> returnVector = { a(0, i), a(1, i), a(2, i), a(3, i), a(4, i), a(5, i), a(6, i) };
				return returnVector;
				//break;
			}
		}
	}
}

vector<double> heavyGasDispersionforCatastrophicRelease(string fileName)
{
	mat loadedValues = loadFromFileV2(fileName);
	vec R_data = loadedValues.col(0);
	//R_data << 1 << 1.3 << 11.6;
	vec dR_data = loadedValues.col(1);
	//dR_data << 60.02 << 60.63 << 85.5;
	vec Ca_data = loadedValues.col(2);
	//Ca_data << 0.156 << 0.156 << 0.14;

	double T_data = 0;
	T_data = T_data * datum::pi / 180;

	int maxIter = 10000;
	double accuracy = 1E-6;

	vector<double> ABCD = heavyGasDispersionforCatastrophicReleaseABCD(R_data, dR_data, T_data, accuracy, maxIter);
	vector<double> EFGHJLM = heavyGasDispersionforCatastrophicReleaseEFGHJLM(R_data, Ca_data, T_data, accuracy, maxIter);
	
	vector<double> returnVector = { ABCD[0], ABCD[1], ABCD[2], ABCD[3], EFGHJLM[0], EFGHJLM[1], EFGHJLM[2], EFGHJLM[3], EFGHJLM[4], EFGHJLM[5], EFGHJLM[6] };
	return returnVector;
}

int main()
{
	//heavyGasDispersionContinuousReleaseV3();
	//heavyGasDispersionforCatastrophicRelease();
	//heavyGasDispersionContinuousReleaseV2();
	//heavyGasDispersionContinuousReleaseV1();
	//gasJetDispersion();
	//poolFire();
	/* // szybkie sprawdzenie dzialania funkcji
	vector<double> wektor;
	//wektor = unconfinedExplosion("file1.uvc");
	//wektor = physicalExplosion("file2.con");
	//wektor = fireball("file3.fir");
	//wektor = jetFire("file4.jet"); // wynik zmiennej c wychodzi rozbie�ny z obliczeniami z matlaba
	//wektor = poolFire();
	//wektor = gasJetDispersion();
	//wektor = heavyGasDispersionContinuousReleaseV1();
	//wektor = heavyGasDispersionContinuousReleaseV2();
	//wektor = heavyGasDispersionContinuousReleaseV3("file7.noa");
	//wektor = heavyGasDispersionforCatastrophicRelease();
	for (int i = 0; i < wektor.size(); i++)
	{
		cout << wektor[i] << " ";
	}
	cout << endl;
	//*/
	
	vector<double> calculatedValues;
	char selector;

	do
	{
		cout << "MENU" << endl;
		cout << "Type number to use function, type e to end program" << endl;
		cout << "1) unconfined explosion" << endl;
		cout << "2) physical explosion" << endl;
		cout << "3) fireball" << endl;
		cout << "4) jet fire" << endl;
		cout << "5) pool fire" << endl; 
		cout << "6) gas jet dispersion" << endl; 
		cout << "7) heavy gas dispersion continuous release" << endl; 
		cout << "8) heavy gas dispersion catastrophic release" << endl << endl;
		cout << "Type: ";
		cin >> selector;

		switch (selector)
		{
			case '1':
				calculatedValues = unconfinedExplosion("file1.uvc");
				break;
			case '2':
				calculatedValues = physicalExplosion("file2.con");
				break;
			case '3':
				calculatedValues = fireball("file3.fir");
				break;
			case '4':
				calculatedValues = jetFire("file4.jet");
				break;
			case '5':
				calculatedValues = poolFire("file5.poo");
				break;
			case '6':
				calculatedValues = gasJetDispersion("file6.oom");
				break;
			case '7':
				calculatedValues = heavyGasDispersionContinuousReleaseV3("file7.noa");
				break;
			case '8':
				calculatedValues = heavyGasDispersionforCatastrophicRelease("file9.den");
				break;
			case 'e':
				break;
			default:
				cout << "There's no such option" << endl;
				break;
		}
	} while(selector != 'e');
	
	return 0;
}